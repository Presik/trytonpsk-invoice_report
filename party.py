# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from trytond.report import Report
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateReport, StateView, Button


class PortfolioDetailedStart(ModelView):
    'Portfolio Detailed Start'
    __name__ = 'invoice_report.portfolio_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    to_date = fields.Date('To Date')
    parties = fields.Many2Many('party.party', None, None, 'Parties')
    type = fields.Selection([
            ('both', 'Both'),
            ('out', 'Customer'),
            ('in', 'Supplier'),
        ], 'Type', required=True)
    salesman = fields.Many2One('company.employee', 'Salesman')
    grouped_by_salesman = fields.Boolean('Grouped By Salesman')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return 'out'


class PortfolioDetailed(Wizard):
    'Portfolio Detailed'
    __name__ = 'invoice_report.portfolio_detailed'
    start = StateView('invoice_report.portfolio_detailed.start',
        'invoice_report.print_portfolio_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('party.portfolio_detailed.report')

    def do_print_(self, action):
        parties_ids = [p.id for p in self.start.parties]
        data = {
            'company': self.start.company.id,
            'type': self.start.type,
            'to_date': self.start.to_date,
            'parties': parties_ids,
        }
        data['field_salesman'] = True
        if hasattr(self.start, 'salesman') and self.start.salesman:
            data['salesman'] = self.start.salesman.id
        if hasattr(self.start, 'grouped_by_salesman'):
            data['grouped_by_salesman'] = self.start.grouped_by_salesman
        return action, data

    def transition_print_(self):
        return 'end'


class PortfolioDetailedReport(Report):
    __name__ = 'party.portfolio_detailed.report'

    @classmethod
    def get_domain_inv(cls, dom_invoices, data):
        states = ['posted']
        if data['to_date']:
            states.append('paid')
        dom_invoices.append([
            ('company', '=', data['company']),
            ('state', 'in', states)
        ])

        if data['parties']:
            dom_invoices.append(
                ('party', 'in', data['parties'])
            )

        if data['type'] in ['in', 'out']:
            dom_invoices.append(
                ('type', '=', data['type'])
            )

        if data['to_date']:
            dom_invoices.append(
                ('invoice_date', '<=', data['to_date']),
            )
        if data.get('salesman'):
            dom_invoices.append(
                ('salesman', '=', data['salesman']),
            )
        return dom_invoices

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        pool = Pool()
        Invoice = pool.get('account.invoice')
        dom_invoices = []
        dom_invoices = cls.get_domain_inv(dom_invoices, data)
        invoices = Invoice.search(dom_invoices,
            order=[('party.name', 'ASC'), ('invoice_date', 'ASC')]
        )
        salesmans = {}
        for invoice in invoices:
            pay_to_date = []
            if data['to_date']:
                if not invoice.move:
                    continue

                move_lines_paid = []
                for line in invoice.payment_lines:
                    if line.move.date <= data['to_date']:
                        pay_to_date.append(line.debit - line.credit)
                        move_lines_paid.append(line.id)
                for line in invoice.move.lines:
                    if not line.reconciliation:
                        continue
                    for recline in line.reconciliation.lines:
                        if recline.id == line.id or line.id in move_lines_paid:
                            continue
                        if recline.move.date <= data['to_date']:
                            pay_to_date.append(recline.debit - recline.credit)

                amount = sum(pay_to_date)
                if invoice.type == 'out':
                    amount *= -1
                if amount >= invoice.total_amount:
                    continue
                amount_to_pay = invoice.total_amount - amount
                invoice.amount_to_pay = amount_to_pay
            else:
                amount_to_pay = invoice.amount_to_pay

            if 'field_salesman' in data.keys() and data.get('grouped_by_salesman', None) and invoice.salesman:
                if invoice.salesman.id not in salesmans.keys():
                    salesmans[invoice.salesman.id] = {
                        'salesman': invoice.salesman.party.full_name,
                        'parties': {},
                        'total_invoices': [],
                        'total_amount_to_pay': [],
                    }

                salesman = invoice.salesman.id
            elif 'without_seller' not in salesmans:
                salesmans['without_seller'] = {
                    'salesman': 'without_seller',
                    'parties': {},
                    'total_invoices': [],
                    'total_amount_to_pay': [],
                }
                salesman = 'without_seller'
            else:
                salesman = 'without_seller'
            if invoice.party.id not in salesmans[salesman]['parties'].keys():
                salesmans[salesman]['parties'][invoice.party.id] = {
                    'party': invoice.party,
                    'invoices': [],
                    'total_invoices': [],
                    'total_amount_to_pay': [],
                }

            salesmans[salesman]['total_invoices'].append(invoice.total_amount)
            salesmans[salesman]['total_amount_to_pay'].append(amount_to_pay)
            salesmans[salesman]['parties'][invoice.party.id]['invoices'].append(invoice)
            salesmans[salesman]['parties'][invoice.party.id]['total_invoices'].append(invoice.total_amount)
            salesmans[salesman]['parties'][invoice.party.id]['total_amount_to_pay'].append(amount_to_pay)

        report_context['records'] = salesmans
        report_context['data'] = data
        return report_context
