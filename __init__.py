# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import company
from . import party


def register():
    Pool.register(
        invoice.Invoice,
        invoice.InvoiceByPeriodStart,
        invoice.InvoiceByPeriodDynamic,
        invoice.OpenInvoiceByPeriodStart,
        invoice.ExpiredPortfolioStart,
        invoice.InvoiceByPartyStart,
        company.Company,
        invoice.InvoicesStart,
        party.PortfolioDetailedStart,
        invoice.InvoiceByProductStart,
        invoice.TaxesCategoryStart,
        module='invoice_report', type_='model')
    Pool.register(
        party.PortfolioDetailed,
        invoice.InvoiceByPeriod,
        invoice.OpenInvoiceByPeriod,
        invoice.ExpiredPortfolio,
        invoice.InvoiceByParty,
        invoice.Invoices,
        invoice.FixInvoice,
        invoice.InvoicesExport,
        invoice.InvoiceByProduct,
        invoice.TaxesCategory,
        module='invoice_report', type_='wizard')
    Pool.register(
        invoice.InvoiceDetailedReport,
        invoice.InvoiceByPeriodReport,
        invoice.InvoiceByPeriodTaxesReport,
        invoice.ExpiredPortfolioReport,
        invoice.InvoiceByPartyReport,
        invoice.PartyDunningReport,
        party.PortfolioDetailedReport,
        invoice.InvoicesReport,
        invoice.InvoicesExportReport,
        invoice.InvoiceByProductReport,
        invoice.TaxesCategoryReport,
        module='invoice_report', type_='report')
