# This file is part of Tryton. The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
import copy
from datetime import date
from decimal import Decimal
from collections import OrderedDict
# from itertools import groupby
import re
from trytond.tools import cursor_dict
from sql.conditionals import Coalesce
from sql.aggregate import Sum, Count
from sql import Column
from trytond.pool import Pool, PoolMeta
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, PYSONEncoder
from trytond.wizard import Wizard, StateView, Button, StateAction, StateReport, StateTransition

_ZERO = Decimal('0.0')


class InvoiceDetailedReport(Report):
    __name__ = 'invoice_report.detailed'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        def get_iva(line):
            amount = 0
            for t in line.taxes:
                if t.classification_tax == '01':
                    amount = float(line.quantity) * float(line.unit_price) * float(t.rate)
                    break
            return amount
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['get_iva'] = get_iva
        return report_context


class InvoiceByPeriodStart(ModelView):
    'Invoice By Period Start'
    __name__ = 'invoice_report.print_invoice_by_period.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    period = fields.Many2One('account.period', 'Period',
            depends=['fiscalyear'], required=True, domain=[
                ('fiscalyear', '=', Eval('fiscalyear')),
            ])
    report = fields.Many2One('ir.action.report', 'Report',
            domain=[
                    ('module', '=', 'invoice_report'),
                    ('report', 'like', '%invoice_by_period%'),
            ], required=True)
    type_inv = fields.Selection([
            ('out', 'Invoice'),
            ('out_credit_note', 'Out Credit Note'),
            ('in', 'Supplier Invoice'),
            ('in_credit_note', 'In Credit Note'),
        ], 'Type', depends=['report', 'report_by_type'], states={
            'invisible': ~Bool(Eval('report_by_type')),
            'required': Bool(Eval('report_by_type')),
        })
    type_inv_string = type_inv.translated('type_inv')
    report_by_type = fields.Boolean('Report by Type')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_report_by_type():
        return False

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.period = None

    @fields.depends('report')
    def on_change_with_report_by_type(self):
        if self.report and \
            self.report.report_name == 'invoice_report.invoice_by_period_report':
            return False
        return True


class InvoiceByPeriod(Wizard):
    'Invoice By Period'
    __name__ = 'invoice_report.print_invoice_by_period'
    start = StateView('invoice_report.print_invoice_by_period.start',
        'invoice_report.print_invoice_by_period_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('invoice_report.invoice_by_period_report')

    def do_print_(self, action):
        type_inv_string = ''
        if self.start.type_inv:
            type_inv_string = self.start.type_inv_string
        data = {
            'ids': [],
            'company': self.start.company.id,
            'period': self.start.period.id,
            'type_invoice': self.start.type_inv,
            'type_inv_string': type_inv_string,
            'fiscalyear': self.start.fiscalyear.id,
        }

        action['report'] =  self.start.report.report
        action['report_name'] = self.start.report.report_name
        action['id'] = self.start.report.id
        action['action'] = self.start.report.action.id
        return action, data

    def transition_print_(self):
        return 'end'


class InvoiceByPeriodReport(Report):
    'Invoice By Period'
    __name__ = 'invoice_report.invoice_by_period_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')
        company = Company(data['company'])
        period = Period(data['period'])

        dperiod = {
            'name': period.name,
            'out_invoice': [],
            'out_credit_note': [],
            'in_invoice': [],
            'in_credit_note': [],
        }

        in_invoices = Invoice.search([
            ('company', '=', data['company']),
            ('move.period', '=', period.id),
            ('type', '=', 'in'),
            ('state', 'in', ['posted', 'paid']),
        ])

        out_invoices = Invoice.search([
            ('company', '=', data['company']),
            ('move.period', '=', period.id),
            ('type', '=', 'out'),
            ('state', 'in', ['posted', 'paid']),
        ])

        sum_total = {
            'out_invoice': [],
            'out_credit_note': [],
            'in_invoice': [],
            'in_credit_note': [],
        }
        for invoice in in_invoices:
            invoice_type = 'in_invoice'
            if invoice.untaxed_amount < 0:
                invoice_type = 'in_credit_note'
            dperiod[invoice_type].append(invoice.untaxed_amount)
            sum_total[invoice_type].append(invoice.untaxed_amount)

        for invoice in out_invoices:
            invoice_type = 'out_invoice'
            if invoice.untaxed_amount < 0:
                invoice_type = 'out_credit_note'
            dperiod[invoice_type].append(invoice.untaxed_amount)
            sum_total[invoice_type].append(invoice.untaxed_amount)

        dperiod['out_invoice'] = sum(dperiod['out_invoice'])
        dperiod['out_credit_note'] = sum(dperiod['out_credit_note'])
        dperiod['in_invoice'] = sum(dperiod['in_invoice'])
        dperiod['in_credit_note'] = sum(dperiod['in_credit_note'])

        for t in sum_total:
            report_context['sum_' + t] = sum(sum_total[t])

        report_context['records'] = [dperiod]
        report_context['fiscalyear'] = Fiscalyear(data['fiscalyear'])
        report_context['company'] = company.party.name

        return report_context


class InvoiceByPeriodTaxesReport(Report):
    'Invoice By Period Taxes'
    __name__ = 'invoice_report.invoice_by_period_taxes_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Tax = pool.get('account.tax')
        Invoice = pool.get('account.invoice')
        Move = pool.get('account.move')
        InvoiceLine = pool.get('account.invoice.line')
        Line_tax = pool.get('account.invoice.line-account.tax')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')
        company = Company(data['company'])
        period = Period(data['period'])

        invoice = Invoice.__table__()
        line = InvoiceLine.__table__()
        tax = Tax.__table__()
        line_tax = Line_tax.__table__()
        move = Move.__table__()

        from_item = line.join(invoice, condition=line.invoice== invoice.id
        ).join(move, condition=invoice.move==move.id
        ).join(line_tax, condition=line_tax.line==line.id
        ).join(tax, condition=tax.id==line_tax.tax)

        where = invoice.company == data['company']
        where &= move.period == data['period']
        where &= invoice.state.in_(['posted', 'paid'])
        where &= line.type == 'line'

        if 'credit' in data['type_invoice']:
            where &= line.quantity <= 0
        else:
            where &= line.quantity >= 0

        if 'out' in data['type_invoice']:
            where &= invoice.type == 'out'
        else:
            where &= invoice.type == 'in'

        columns = [
            move.period,
            Sum(line.quantity*line.unit_price*tax.rate).as_('taxes'),
            Sum(line.quantity*line.unit_price).as_('base_taxed'),
            Count(line.quantity).as_('num_lines'),
        ]

        query = from_item.select(
            *columns,
            where=(tax.classification == 'iva')
            & where,
            group_by=(move.period)
        )
        result = cls.query_to_dict(query)
        result = list(result.values())
        columns = [
            move.period,
            Sum(invoice.untaxed_amount_cache).as_('base_amount'),
            Count(line.quantity).as_('num_lines'),
        ]

        dperiod = {
            'name': period.name,
            'base_taxed': [],
            'base_untaxed': [],
            'taxes': [],
            'total': [],
        }

        if 'out' in data['type_invoice']:
            type_ = 'out'
        else:
            type_ = 'in'

        type_invoice = data['type_invoice']
        if 'credit' in type_invoice:
            condition = ('untaxed_amount_cache', '<=', 0)
        else:
            condition = ('untaxed_amount_cache', '>=', 0)


        invoices = Invoice.search_read([
            ('company', '=', data['company']),
            ('move.period', '=', data['period']),
            ('state', 'in', ['posted', 'paid']),
            ('type', '=', type_),
            ('move', '!=', None),
            condition,
        ], fields_names=['untaxed_amount_cache'])

        total_untaxed = sum(i['untaxed_amount_cache'] for i in invoices)

        dperiod['base_taxed'] = Decimal(result[0]['base_taxed'])
        dperiod['taxes'] = Decimal(result[0]['taxes'])
        dperiod['base_untaxed'] = total_untaxed - dperiod['base_taxed']

        report_context['records'] = [dperiod]
        report_context['fiscalyear'] = Fiscalyear(data['fiscalyear'])
        report_context['type_invoice'] = data['type_inv_string']
        report_context['company'] = company.party.name
        return report_context

    @classmethod
    def query_to_dict(cls, query):
        cursor = Transaction().connection.cursor()
        cursor.execute(*query)
        columns = list(cursor.description)
        result = cursor.fetchall()
        res_dict = {}
        for row in result:
            row_dict = {}
            for i, col in enumerate(columns):
                row_dict[col.name] = row[i]
            res_dict[row[0]] = row_dict
        return res_dict


class OpenInvoiceByPeriodStart(ModelView):
    'Open Invoice By Period Start'
    __name__ = 'invoice_report.open_invoice_by_period.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)


class OpenInvoiceByPeriod(Wizard):
    'Open Invoice By Period'
    __name__ = 'invoice_report.open_invoice_by_period'
    start = StateView('invoice_report.open_invoice_by_period.start',
        'invoice_report.open_invoice_by_period_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'open_', 'tryton-ok', default=True),
        ])
    open_ = StateAction('invoice_report.act_invoice_by_period_board')

    def do_open_(self, action):
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
        }
        action['name'] += ' - %s' % self.start.fiscalyear.name
        action['pyson_context'] = PYSONEncoder().encode({
            'fiscalyear': self.start.fiscalyear.id,
        })

        return action, data

    def transition_open_(self):
        return 'end'


class InvoiceByPeriodDynamic(ModelSQL, ModelView):
    'Invoice By Period Dynamic'
    __name__ = 'invoice_report.invoice_by_period_dynamic'
    name = fields.Char('Name')
    invoices = fields.Function(fields.One2Many('account.invoice', None,
        'Invoices'), 'get_invoices')
    supplier_invoices = fields.Function(fields.One2Many('account.invoice',
            None, 'Supplier Invoices'), 'get_invoices')
    amount_invoices = fields.Function(fields.Numeric('Amount Invoices',
            digits=(16, 2)), 'get_amount')
    amount_supplier_invoices = fields.Function(fields.Numeric('Amount Supplier Invoices',
            digits=(16, 2)), 'get_amount')

    @classmethod
    def __setup__(cls):
        super(InvoiceByPeriodDynamic, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @classmethod
    def table_query(cls):
        Period = Pool().get('account.period')
        period = Period.__table__()
        columns = []
        fiscalyear_id = Transaction().context.get('fiscalyear')
        for fname, field in cls._fields.items():
            if not hasattr(field, 'set'):
                columns.append(Column(period, fname).as_(fname))
        return period.select(*columns,
                where=(period.type == 'standard') & (period.fiscalyear == fiscalyear_id)
            )

    def get_amount(self, name=None):
        if name == 'amount_invoices':
            invoices = self.invoices
        else:
            invoices = self.supplier_invoices
        return sum([invoice.untaxed_amount for invoice in invoices])

    def get_invoices(self, name=None):
        Invoice = Pool().get('account.invoice')
        if name == 'supplier_invoices':
            type_ = 'in'
        else:
            type_ = 'out'
        invoices = Invoice.search([
            ('move.period', '=', self.id),
            ('type', '=', type_),
            ('state', 'in', ['paid', 'posted', 'validated']),
        ])
        return [i.id for i in invoices]


class ExpiredPortfolioStart(ModelView):
    'Expired Portfolio Start'
    __name__ = 'invoice_report.print_expired_portfolio.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    group = fields.Selection([
            ('customers', 'Customers'),
            ('suppliers', 'Suppliers'),
        ], 'Group', required=True)
    group_string = group.translated('group')
    detailed = fields.Boolean('Detailed')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_group():
        return 'customers'

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)


class ExpiredPortfolio(Wizard):
    'Invoice By Period'
    __name__ = 'invoice_report.print_expired_portfolio'
    start = StateView('invoice_report.print_expired_portfolio.start',
        'invoice_report.print_expired_portfolio_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('invoice_report.expired_portfolio_report')

    def do_print_(self, action):

        # 'detailed': self.start.detailed
        data = {
            'ids': [],
            'company': self.start.company.id,
            'group': self.start.group,
            'group_string': self.start.group_string,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ExpiredPortfolioReport(Report):
    'Expired Portfolio Report'
    __name__ = 'invoice_report.expired_portfolio_report'

    @classmethod
    def get_notes(cls, invoice):
        return False

    @classmethod
    def get_domain_invoice(cls, data):
        domain = [
            ('company', '=', data['company']),
            ('state', '=', 'posted'),
        ]
        return domain

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        company = Company(data['company'])

        dom_invoices = cls.get_domain_invoice(data)

        if data['group'] == 'customers':
            type_ = 'out'
        else:
            type_ = 'in'

        dom_invoices.append(('type', '=', type_))

        order = [('party', 'DESC'), ('invoice_date', 'ASC')]
        invoices = Invoice.search(dom_invoices, order=order)
        parties = {}
        today = date.today()
        expired_kind = {
            'range_0': [],
            'range_1_30': [],
            'range_31_60': [],
            'range_61_90': [],
            'range_91': [],
        }
        expired_sums = copy.deepcopy(expired_kind)
        expired_sums['total'] = []
        for invoice in invoices:
            _expired_kind = copy.deepcopy(expired_kind)
            if invoice.party.id not in parties.keys():
                parties[invoice.party.id] = {
                    'name': invoice.party.name,
                    'total': [],
                    'notes': ''
                }
                parties[invoice.party.id].update(_expired_kind)
            # FIX ME the invoices can different expired dates and the range days
            # is variable
            time_forward = (today - invoice.invoice_date).days
            if time_forward <= 0:
                expire_time = 'range_0'
            elif time_forward <= 30:
                expire_time = 'range_1_30'
            elif time_forward <= 60:
                expire_time = 'range_31_60'
            elif time_forward <= 90:
                expire_time = 'range_61_90'
            else:
                expire_time = 'range_91'

            notes = cls.get_notes(invoice)
            if hasattr(invoice, 'agent') and invoice.agent:
                notes = invoice.agent.rec_name
            if notes and not parties[invoice.party.id]['notes']:
                parties[invoice.party.id]['notes'] = notes
            amount = invoice.amount_to_pay
            parties[invoice.party.id][expire_time].append(amount)
            parties[invoice.party.id]['total'].append(amount)
            expired_sums[expire_time].append(amount)
            expired_sums['total'].append(amount)

        parties_dict = OrderedDict(sorted({p['name']: p for p in parties.values()}.items()))
        report_context.update(expired_sums)
        report_context['records'] = parties_dict
        report_context['type_portfolio'] = data['group_string']
        report_context['company'] = company.party.name
        return report_context


class InvoiceByPartyStart(ModelView):
    'Open Invoice By Party Start'
    __name__ = 'invoice_report.print_invoice_by_party.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    parties = fields.Many2Many('party.party', None, None, 'Party')
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    kind = fields.Selection([
            ('invoice', 'Invoice'),
            ('income', 'Income'),
        ], 'Kind')
    taxes_included = fields.Boolean('Taxes Included')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_kind():
        return 'invoice'

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)


class InvoiceByParty(Wizard):
    'Open Invoice By Party'
    __name__ = 'invoice_report.print_invoice_by_party'
    start = StateView('invoice_report.print_invoice_by_party.start',
        'invoice_report.print_invoice_by_party_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'open_', 'tryton-ok', default=True),
    ])
    open_ = StateReport('invoice_report.invoice_by_party_report')

    def do_open_(self, action):
        parties = [p.id for p in self.start.parties]
        data = {
            'ids': [],
            'company': self.start.company.id,
            'parties': parties,
            'fiscalyear': self.start.fiscalyear.id,
            'kind': self.start.kind,
            'taxes_included': self.start.taxes_included,
        }
        return action, data

    def transition_open_(self):
        return 'end'


class InvoiceByPartyReport(Report):
    'Invoice By Party Report'
    __name__ = 'invoice_report.invoice_by_party_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        MoveLine = pool.get('account.move.line')
        Fiscalyear = pool.get('account.fiscalyear')
        company = Company(data['company'])
        kind = data['kind']

        months = {}
        for i in range(12):
            i += 1
            months[str(i)] = []

        def _get_months():
            return dict([(str(i + 1), _ZERO) for i in range(12)])

        sum_total = []
        parties = {}
        if kind == 'invoice':
            domain = [
                ('company', '=', data['company']),
                ('move.period.fiscalyear', '=', data['fiscalyear']),
                ('state', 'in', ['validated', 'posted', 'paid']),
                ('type', '=', 'out'),
            ]
            if data.get('parties'):
                domain.append(('party', 'in', data.get('parties')))
            invoices = Invoice.search(domain, order=[('party.name', 'ASC')])
            for invoice in invoices:
                if invoice.party.id not in parties.keys():
                    parties[invoice.party.id] = {
                        'name': invoice.party.rec_name,
                        'total': _ZERO,
                    }
                    parties[invoice.party.id].update(_get_months())
                month = str(invoice.invoice_date.month)
                if data['taxes_included']:
                    amount = invoice.total_amount
                else:
                    amount = invoice.untaxed_amount

                parties[invoice.party.id][month] += amount
                parties[invoice.party.id]['total'] += amount
                months[month].append(amount)
                sum_total.append(amount)
        else:
            domain = [
                ('move.period.fiscalyear', '=', data['fiscalyear']),
                ('move.state', '=', 'posted'),
                ('account.type.payable', '=', True),
                ('credit', '>=', Decimal(0)),
            ]
            if data.get('parties'):
                domain.append(('party', 'in', parties))
            lines = MoveLine.search(domain)
            for line in lines:
                amount = line.credit
                month = str(line.move.date.month)
                if line.party.id not in parties.keys():
                    parties[line.party.id] = {
                        'name': line.party.rec_name,
                        'total': _ZERO,
                    }
                    parties[line.party.id].update(_get_months())
                parties[line.party.id][month] += amount
                parties[line.party.id]['total'] += amount
                months[month].append(amount)
                sum_total.append(amount)

        for k, v in months.items():
            report_context['sum_' + k] = sum(v)

        report_context['records'] = sorted(parties.values(),
                key=lambda val: val['name'])
        report_context['kind'] = kind
        report_context['sum_total'] = sum(sum_total)
        report_context['fiscalyear'] = Fiscalyear(data['fiscalyear'])
        report_context['taxes_included'] = data['taxes_included']
        report_context['company'] = company.party.name
        return report_context

    @classmethod
    def _get_amount(cls, kind, record):
        if kind == 'invoice':
            month = str(record.invoice_date.month)
            amount = record.total_amount
        else:
            month = str(record.move.date.month)
            amount = record.credit - record.debit
        return month, amount


class PartyDunningReport(Report):
    __name__ = 'invoice_report.party_dunning'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        company_id = Transaction().context.get('company')
        _records = []
        for party in records:
            invoices = Invoice.search([
                ('party', '=', party.id),
                ('state', 'in', ['posted', 'validated']),
                ('type', '=', 'out'),
                ('move', '!=', None),
                ], order=[('invoice_date', 'ASC'), ('number', 'ASC')]
            )

            posted_invoices = []
            sum_invoices = []
            sum_amount_to_pay = []
            move_invoice_ids = []
            sum_inv_append = sum_invoices.append
            posted_inv_append = posted_invoices.append
            sum_amount_pay_append = sum_amount_to_pay.append
            move_ind_append = move_invoice_ids.append
            for invoice in invoices:
                sum_inv_append(invoice.total_amount)
                sum_amount_pay_append(invoice.amount_to_pay)
                posted_inv_append(invoice)
                move_ind_append(invoice.move.id)

            setattr(party, 'invoices', posted_invoices)
            setattr(party, 'sum_invoices', sum(sum_invoices))
            setattr(party, 'sum_amount_to_pay', sum(sum_amount_to_pay))
            cond2 = 'and pp.id=%s ' % (party.id)
            if move_invoice_ids:
                cond2 += 'and ml.move not in %s' % (str(tuple(move_invoice_ids)).replace(',', '') if len(move_invoice_ids) == 1 else str(tuple(move_invoice_ids)))

            cursor = Transaction().connection.cursor()
            query = f"""select ml.id, ml.move, pp.name, pp.id_number, ml.description,
            am.date, ml.maturity_date, (current_date-coalesce(ml.maturity_date, am.date)::date) as expired_days, coalesce(sum(av.amount), 0) as payment_amount,
            ((ml.debit-ml.credit) - coalesce(sum(av.amount), 0)) as total,
            (ml.debit-ml.credit) as amount
            from account_move_line as ml
            left join account_account as ac on ml.account = ac.id
            join account_account_type as at on ac.type = at.id left
            join account_voucher_line as av on ml.id=av.move_line
            left join account_move as am on am.id=ml.move
            join party_party as pp on pp.id=ml.party
            where at.receivable='t' and ac.reconcile='t' and (am.origin is null or am.origin like 'account.note%%') and ml.reconciliation is null %s
            group by ml.id, ml.move, pp.name, pp.id_number, ml.description, am.date, ml.maturity_date, expired_days, ml.debit, ml.credit;""" % (cond2)
            cursor.execute(query)
            columns = list(cursor.description)
            result = cursor.fetchall()
            move_lines_without_invoice = {}
            amount_pend = []
            for row in result:
                if row[10] <= 0:
                    continue
                row_dict = {}
                for i, col in enumerate(columns):
                    if col.name == 'total':
                        amount_pend.append(row[i])
                    row_dict[col.name] = row[i]
                move_lines_without_invoice[row[0]] = row_dict

            setattr(party, 'moves_without_invoice', move_lines_without_invoice.values())
            party.sum_amount_to_pay += sum(amount_pend)
            _records.append(party)
        report_context['records'] = _records
        report_context['company'] = Company(company_id)
        return report_context


class InvoicesStart(ModelView):
    'Invoices Start'
    __name__ = 'invoice_report.print_invoices.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
            depends=['fiscalyear'], required=True, domain=[
                ('fiscalyear', '=', Eval('fiscalyear')),
            ])
    end_period = fields.Many2One('account.period', 'End Period',
        depends=['fiscalyear'], required=True, domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ])
    type_inv = fields.Selection([
            ('out', 'Invoice'),
            ('out_credit_note', 'Invoice Credit Note'),
            ('in', 'Supplier Invoice'),
            ('in_credit_note', 'Supplier Credit Note'),
            ('payments', 'Payments')
        ], 'Type', required=True)
    payment_terms = fields.Many2Many('account.invoice.payment_term', None, None,
        'Payment Terms')
    parties = fields.Many2Many('party.party', None, None, 'Party')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class Invoices(Wizard):
    'Invoices'
    __name__ = 'invoice_report.print_invoices'
    start = StateView(
        'invoice_report.print_invoices.start',
        'invoice_report.print_invoices_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('invoice_report.invoices_report')

    def do_print_(self, action):
        payment_terms_ids = [p.id for p in self.start.payment_terms]
        parties_ids = [p.id for p in self.start.parties]
        data = {
            'ids': [],
            'company': self.start.company.id,
            'company_name': self.start.company.rec_name,
            'start_period': self.start.start_period.id,
            'start_period_name': self.start.start_period.name,
            'end_period': self.start.end_period.id,
            'end_period_name': self.start.end_period.name,
            'type_invoice': self.start.type_inv,
            'fiscalyear': self.start.fiscalyear.id,
            'payment_terms': payment_terms_ids,
            'parties': parties_ids,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class InvoicesReport(Report):
    'Invoices Report'
    __name__ = 'invoice_report.invoices_report'

    @classmethod
    def get_domain_invoices(cls, data):
        Period = Pool().get('account.period')
        type_invoice = data['type_invoice']
        if data['type_invoice'] == 'out_credit_note':
            type_invoice = 'out'
        elif data['type_invoice'] == 'in_credit_note':
            type_invoice = 'in'
        start_period = Period(data['start_period'])
        end_period = Period(data['end_period'])
        start_periods = Period.search([
                ('fiscalyear', '=', data['fiscalyear']),
                ('start_date', '>=', start_period.start_date),
                ('end_date', '<=', end_period.end_date),
            ])
        dom_invoices = [
            ('company', '=', data['company']),
            ('move.period', 'in', [p.id for p in start_periods]),
            ('type', '=', type_invoice),
            ('state', 'in', ['posted', 'paid']),
        ]
        if data['payment_terms']:
            dom_invoices.append(
                ('payment_term', 'in', data['payment_terms']),
            )
        if data.get('parties'):
            dom_invoices.append(
                ('party', 'in', data['parties']),
            )
        return dom_invoices

    @classmethod
    def get_record(cls, invoice):
        invoice = {
            'date': invoice.invoice_date,
            'reference': invoice.reference,
            'payment_term': invoice.payment_term,
            'number': invoice.number,
            'party': invoice.party.rec_name,
            'id_number': invoice.party.id_number,
            'description': invoice.description,
            'iva': 0,
            'ret': 0,
            'ica': 0,
            'ic': 0,
            'untaxed_amount': 0,
            'taxed_amount': 0,
            'total_amount': invoice.total_amount,
            'state': invoice.state_string
        }
        return invoice

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoices = Invoice.search(cls.get_domain_invoices(data),
            order=[('invoice_date', 'ASC')]
        )

        sum_untaxed_amount = 0
        sum_total_amount = 0
        invoices_filtered = OrderedDict()
        tax_totals = {
            'total_iva': 0,
            'total_ret': 0,
            'total_ica': 0,
            'total_ic': 0,
        }

        get_record = cls.get_record
        for invoice in invoices:
            total_amount = invoice.total_amount
            if data['type_invoice'] in ('out_credit_note', 'in_credit_note'):
                if total_amount >= 0:
                    continue
            inv_id = invoice.id
            invoices_filtered[inv_id] = get_record(invoice)
            taxed_amount = 0
            for inv_tax in invoice.taxes:
                amount_ = inv_tax.amount
                classification = inv_tax.tax.classification
                if inv_tax.tax.classification_tax == '01':
                    taxed_amount += inv_tax.base

                if classification:
                    try:
                        invoices_filtered[inv_id][classification] += amount_
                        tax_totals['total_' + classification] += amount_
                    except:
                        pass
            invoices_filtered[inv_id]['untaxed_amount'] = invoice.untaxed_amount - taxed_amount
            invoices_filtered[inv_id]['taxed_amount'] = taxed_amount
            sum_untaxed_amount += invoice.untaxed_amount
            sum_total_amount += invoice.total_amount

        report_context['records'] = invoices_filtered.values()
        report_context['sum_total_iva'] = tax_totals['total_iva']
        report_context['sum_total_ret'] = tax_totals['total_ret']
        report_context['sum_total_ica'] = tax_totals['total_ica']
        report_context['sum_total_ic'] = tax_totals['total_ic']
        report_context['sum_untaxed_amount'] = sum_untaxed_amount
        report_context['sum_total_amount'] = sum_total_amount
        report_context['invoices_filtered'] = invoices_filtered
        report_context['company'] = data['company_name']

        return report_context


class TaxesCategoryStart(ModelView):
    'Taxes Category Start'
    __name__ = 'invoice_report.print_tax_categories.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
            depends=['fiscalyear'], required=True, domain=[
                ('fiscalyear', '=', Eval('fiscalyear')),
            ])
    end_period = fields.Many2One('account.period', 'End Period',
            depends=['fiscalyear'], required=True, domain=[
                ('fiscalyear', '=', Eval('fiscalyear')),
            ])
    type_inv = fields.Selection([
            ('out', 'Invoice'),
            ('out_dev', 'Note Invoice'),
            ('in', 'Supplier Invoice'),
            ('in_dev', 'Note Supplier Invoice'),

        ], 'Type', required=True)
    categories = fields.Many2Many('product.category', None, None, 'Categories',
            domain=[('accounting', '=', 'true')])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None

    @fields.depends('start_period')
    def on_change_start_period(self):
        self.end_period = self.start_period


class TaxesCategory(Wizard):
    'tax categories'
    __name__ = 'invoice_report.print_tax_categories'
    start = StateView(
        'invoice_report.print_tax_categories.start',
        'invoice_report.print_tax_categories_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('invoice_report.tax_category_report')

    def do_print_(self, action):
        pool = Pool()
        ProductCategory = pool.get('product.category')
        if self.start.categories:
            categories = [key.id for key in self.start.categories]
        else:
            category = ProductCategory.search([('accounting', '=', 'true')])
            categories = [categorie.id for categorie in category]
        data = {
            'ids': [],
            'company': self.start.company.id,
            'company_name': self.start.company.rec_name,
            'start_period': self.start.start_period.start_date,
            'start_period_name': self.start.start_period.name,
            'end_period': self.start.start_period.start_date,
            'end_period_name': self.start.start_period.name,
            'type_invoice': self.start.type_inv,
            'fiscalyear': self.start.fiscalyear.id,
            'categories': categories,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class TaxesCategoryReport(Report):
    'Taxes Category Report'
    __name__ = 'invoice_report.tax_category_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Move = pool.get('account.move')
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        Move = pool.get('account.move')
        Product = pool.get('product.product')
        ProductTemplate = pool.get('product.template')
        ProductCategory = pool.get('product.category')
        Period = pool.get('account.period')
        i = Invoice.__table__()
        v = InvoiceLine.__table__()
        m = Move.__table__()
        p = Product.__table__()
        t = ProductTemplate.__table__()
        c = ProductCategory.__table__()
        d = Period.__table__()
        if data['categories']:
            categories = list(data['categories'])
        if data['type_invoice'] in ('in', 'out'):
            where = (v.quantity > 0)
            type_invoice = 'in'
        if data['type_invoice'] in ('in_dev', 'out_dev'):
            where = (v.quantity < 0)
            type_invoice = 'out'

        columns = []
        query = (v
            .join(i, condition=v.invoice == i.id)
            .join(m, condition=i.move == m.id)
            .join(d, condition=m.period == d.id)
            .join(p, condition=v.product == p.id)
            .join(t, condition=p.template == t.id)
            .join(c, condition=t.account_category == c.id)
            .select(c.name, c.id,
                Sum((v.unit_price * v.quantity)).as_('amount'),
                Sum(Coalesce(p.extra_tax, 0) * v.quantity).as_('impc'),
                *columns, where=(i.type == type_invoice)
                & (d.start_date >= data['start_period'])
                & (d.start_date <= data['end_period'])
                & t.account_category.in_(categories)
                & i.state.in_(['posted', 'paid'])
                & where,
                group_by=c.id)
            )

        cursor.execute(*query)

        tax_categories = cursor_dict(cursor)

        sum_untaxed_amount = 0
        categories_filtered = OrderedDict()
        tax_totals = {
            'total_iva': 0,
            'total_ret': 0,
            'total_ica': 0,
            'total_ic': 0,
        }
        for row in tax_categories:
            print(row['amount'], 'amount positive or negative')
            category_id = row['id']
            categories_filtered[category_id] = {
                'name': row['name'],
                'iva': 0,
                'ret': 0,
                'ica': 0,
                'ic': row['impc'],
                'untaxed_amount': row['amount'],
            }

            category, = ProductCategory.search([('id', '=', category_id)])
            if data['type_invoice'] == 'in':
                taxes = [tax for tax in category.supplier_taxes]
            else:
                taxes = [tax for tax in category.customer_taxes]
            for tax in taxes:
                amount_ = row['amount']
                classification = tax.classification

                if classification and tax.type == 'percentage':
                    tax_amount = float((Decimal(amount_)*tax.rate))
                    categories_filtered[category_id][classification] += tax_amount
                    tax_totals['total_' + classification] += tax_amount

            tax_totals['total_' + 'ic'] += row['impc']
            sum_untaxed_amount += row['amount']

        report_context['sum_total_iva'] = tax_totals['total_iva']
        report_context['sum_total_ret'] = tax_totals['total_ret']
        report_context['sum_total_ica'] = tax_totals['total_ica']
        report_context['sum_total_ic'] = tax_totals['total_ic']
        report_context['sum_untaxed_amount'] = sum_untaxed_amount
        report_context['categories_filtered'] = categories_filtered
        report_context['company'] = data['company_name']

        return report_context


class FixInvoice(Wizard):
    'Fix Invoice Test'
    __name__ = 'account.invoice.fix_seq'
    start_state = 'fix_seq'
    fix_seq = StateTransition()

    def transition_fix_seq(self):
        Invoice = Pool().get('account.invoice')
        # shop = Pool().get('sale.shop')(1)
        Journal = Pool().get('account.journal')
        # Party = Pool().get('party.party')
        Sale = Pool().get('sale.sale')
        invoices = Invoice.search_read([
            ('number', '!=', None),
            ('type', '=', 'out'),
            ('invoice_date', '<=', '2017-12-31'),
            ], fields_names=['number'])
        invoice_numbers = [i['number'] for i in invoices]
        sales = Sale.search([
            ('number', '!=', None),
            ('number', 'not like', 'DEV%'),
            ('state', 'not in', ['done', 'cancel']),
            ('sale_date', '>=', '2017-01-01'),
            ('sale_date', '<=', '2017-03-31'),
        ], order=[('number', 'ASC')])

        missing = [s for s in sales if not s.invoices]

        journal, = Journal.search([
            ('type', '=', 'revenue')
        ], limit=1)
        # party_id = shop.party.id
        # invoice_address = Party.address_get(shop.party, type='invoice')
        journal_id = journal.id

        for sale in missing:
            if sale.number in invoice_numbers:
                continue
            print('Creating sequence....,', sale.number)
            Invoice.create([{
                'number': sale.number,
                'party': sale.party.id,
                'invoice_address': sale.party.addresses[0].id,
                'invoice_date': sale.sale_date,
                'state': 'cancel',
                'company': 1,
                'type': 'out',
                'journal': journal_id,
                'account': 350,
                'comment': 'AUTO',
            }])
            sale.write([sale], {'state': 'cancel'})
        return 'end'


class InvoicesExport(Wizard):
    'Invoices Export'
    __name__ = 'invoice_report.print_invoices_export'
    start = StateView(
        'invoice_report.print_invoices.start',
        'invoice_report.print_invoices_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('invoice_report.invoices_export_report')

    def do_print_(self, action):
        payment_terms_ids = [p.id for p in self.start.payment_terms]
        parties_ids = [p.id for p in self.start.parties]
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_period': self.start.start_period.id,
            'start_period_name': self.start.start_period.name,
            'end_period': self.start.end_period.id,
            'end_period_name': self.start.end_period.name,
            'type_invoice': self.start.type_inv,
            'fiscalyear': self.start.fiscalyear.id,
            'payment_terms': payment_terms_ids,
            'parties': parties_ids,
            'shop': self.start.shop.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class InvoicesExportReport(Report):
    'Invoices Export Report'
    __name__ = 'invoice_report.invoices_export_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        Period = pool.get('account.period')
        Statement = pool.get('account.statement')
        period_start = Period(data['start_period'])
        period_end = Period(data['end_period'])

        if data['type_invoice'] == 'payments':
            statements = Statement.search([
                ('company', '=', data['company']),
                ('date', '>=', period_start.start_date),
                ('date', '<=', period_end.end_date),
            ])
            moves = [l.move for s in statements for l in s.lines]
            objects = []
            for move in moves:
                if move:
                    seq = 1
                    year = move.date.year
                    month = move.date.month
                    day = move.date.day
                    for line in move.lines:
                        id_number = ''
                        if line.party:
                            id_number = line.party.id_number
                        if line.debit > 0:
                            dc = 'D'
                            amount = line.debit
                        else:
                            dc = 'C'
                            amount = line.credit
                        objects.append({
                            'tipo': 'R',
                            'code': '0',
                            'number': line.move.number,
                            'account_code': line.account.code,
                            'dc': dc,
                            'amount': amount,
                            'year': year,
                            'month': month,
                            'day': day,
                            'seq': seq,
                            'id_number': id_number,
                            'desc': line.description,
                            'tax_rate': 0,
                            'tax_amount': 0,
                            'prod_grav': '',
                        })
                        seq += 1
        else:
            # if data['type_invoice'] == "out_credit_note":
            #     type_invoice = 'out'
            # elif data['type_invoice'] == "in_credit_note":
            #     type_invoice = 'in'
            # else:
            type_invoice = data['type_invoice']
            dom_invoices = [
                ('company', '=', data['company']),
                ('move.period', 'in', [data['start_period'], data['end_period']]),
                ('type', '=', type_invoice),
                ('state', 'in', ['posted', 'paid']),
                ('shop', '=', data.get('shop')),
            ]

            if data['payment_terms']:
                dom_invoices.append(
                    ('payment_term', 'in', data['payment_terms']),
                )
            if data['parties']:
                dom_invoices.append(
                    ('party', 'in', data['parties']),
                )
            if data.get('shop', None):
                dom_invoices.append(
                    ('shop', '=', data.get('shop')),
                )
            invoices = Invoice.search(dom_invoices)

            objects = []
            # invoices_filtered = []
            # for invoice in invoices:
            #     if invoice.untaxed_amount < _ZERO:
            #         if "credit" in data['type_invoice']:
            #             invoices_filtered.append(invoice)
            #         else:
            #             continue
            #     else:
            #         if "credit" not in data['type_invoice']:
            #             invoices_filtered.append(invoice)
            #         else:
            #             continue

            for invoice in invoices:
                seq = 1
                year = invoice.invoice_date.year
                month = invoice.invoice_date.month
                day = invoice.invoice_date.day
                id_number = invoice.party.id_number
                number = invoice.number
                prefix = re.split('(\d+)', number)[0]
                number_only = re.split('(\d+)', number)[1]
                num = 0
                for line in invoice.move.lines:
                    # if line
                    invoice_lines = InvoiceLine.search([
                        ('invoice', '=', invoice),
                    ])
                    # if line.account.startswith('41') == True:

                    if line.debit > 0:
                        dc = 'D'
                        amount = line.debit
                    else:
                        dc = 'C'
                        amount = line.credit
                    tax_rate = 0
                    tax_amount = 0
                    prod_grav = ''
                    if line.tax_lines:
                        tl = line.tax_lines[0]
                        tax_rate = tl.tax.rate * 100
                        tax_amount = tl.amount
                        prod_grav = 'S'
                    description = ''

                    try:
                        if line.account.code.startswith('41') == True:
                            description = invoice_lines[num].product.name
                            num = num+1
                    except:
                        description = line.description

                    objects.append({
                        'tipo': 'F',
                        'code': '0',
                        'number': number,
                        'prefix': prefix,
                        'number_only': number_only,
                        'account_code': line.account.code,
                        'dc': dc,
                        'amount': amount,
                        'year': year,
                        'month': month,
                        'day': day,
                        'seq': seq,
                        'id_number': id_number,
                        'desc': description,
                        'tax_rate': tax_rate,
                        'tax_amount': tax_amount,
                        'prod_grav': prod_grav,
                    })
                seq += 1

        report_context['records'] = objects
        return report_context


class InvoiceByProductStart(ModelView):
    'Invoice By Product Start'
    __name__ = 'invoice_report.invoice_by_product.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    party = fields.Many2One('party.party', 'Party')
    product = fields.Many2One('product.product', 'Product')
    type = fields.Selection([
        ('', ''),
        ('in', 'Supplier'),
        ('out', 'Customer')], 'Type', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class InvoiceByProduct(Wizard):
    'Invoice By Product'
    __name__ = 'invoice_report.invoice_by_product.wizard'
    start = StateView(
        'invoice_report.invoice_by_product.start',
        'invoice_report.invoice_by_product_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateAction('invoice_report.invoice_by_product_report_')

    @classmethod
    def __setup__(cls):
        super(InvoiceByProduct, cls).__setup__()

    def do_print_(self, action):
        party_id = None
        product_id = None
        if self.start.party:
            party_id = self.start.party.id
        if self.start.product:
            product_id = self.start.product.id
        data = {
            'ids': [],
            'company': self.start.company.id,
            'company_name': self.start.company.rec_name,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'party': party_id,
            'product': product_id,
            'type': self.start.type,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class InvoiceByProductReport(Report):
    __name__ = 'invoice_report.invoice_by_product_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Company = pool.get('company.company')

        invoice_line_dom = [
            ('invoice.invoice_date', '>=', data['start_date']),
            ('invoice.invoice_date', '<=', data['end_date']),
            ('invoice.company', '=', data['company']),
            ('invoice.state', 'in', ['posted', 'paid']),
            ('type', '=', 'line'),
            ('product', '!=', None),
            ('invoice.type', '=', 'out'),
        ]
        if data['party']:
            invoice_line_dom.append(
                ('invoice.party', '=', data['party'])
            )
        if data['product']:
            invoice_line_dom.append(
                ('product', '=', data['product'])
            )
        invoice_lines = InvoiceLine.search(invoice_line_dom)
        lines = {}
        sum_total_amount = 0
        for line in invoice_lines:
            product_id = line.product.id
            if product_id not in lines.keys():
                lines[product_id] = {
                    'product': line.product.rec_name,
                    'quantity': line.quantity,
                    'amount': line.amount,
                }
                sum_total_amount += line.amount
            else:
                lines[product_id]['quantity'] += line.quantity
                lines[product_id]['amount'] += line.amount
                sum_total_amount += line.amount

        print(data['start_date'])
        report_context['records'] = lines.values()
        report_context['sum_total_amount'] = sum_total_amount
        report_context['company'] = Company(data['company'])
        report_context['company_name'] = data['company_name']
        return report_context


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    aged = fields.Function(fields.Integer('Aged'), 'get_aged')

    def get_aged(self, name=None):
        res = None
        if self.invoice_date:
            res = (date.today() - self.invoice_date).days
        return res

    # @classmethod
    # def import_data(cls, fields_names, data):
    #     # IMPORT FROM PAQ GOSII
    #     pool = Pool()
    #     InvoiceLine = pool.get('account.invoice.line')
    #     Account = pool.get('account.account')
    #     Tax = pool.get('account.tax')
    #     Invoice = pool.get('account.invoice')
    #     Party = pool.get('party.party')
    #     Product = pool.get('product.product')
    #     PaymentTerm = pool.get('account.invoice.payment_term')
    #     CountryCode = pool.get('party.country_code')
    #     Country = pool.get('country.country')
    #     InvoiceTax = pool.get('account.invoice.tax')
    #
    #     count = 0
    #     invoices_dict = {}
    #     parties_dict = {}
    #     count_records = []
    #     item_products = Product.search([
    #         ('name', '=', 'MUEBLE')
    #     ])
    #     item_product = item_products[0]
    #     journal_id = 1
    #     payment_term = PaymentTerm(1)
    #     account, = Account.search([
    #         ('code', '=', '130505')
    #     ])
    #     account_id = account.id
    #     taxes_iva = Tax.search([
    #         ('rate', '=', Decimal('0.19')),
    #         ('type', '=', 'percentage'),
    #         ('group.kind', '=', 'sale'),
    #     ])
    #     taxes_ret4 = Tax.search([
    #         ('rate', '=', Decimal('-0.04')),
    #         ('type', '=', 'percentage'),
    #         ('group.kind', '=', 'sale'),
    #     ])
    #     taxes_ret2 = Tax.search([
    #         ('rate', '=', Decimal('-0.025')),
    #         ('type', '=', 'percentage'),
    #         ('group.kind', '=', 'sale'),
    #     ])
    #     taxes_ica = Tax.search([
    #         ('name', '=','ICA 10 X MIL'),
    #     ])
    #
    #     def text2numeric(val):
    #         val = val.replace('.', '')
    #         val = val.replace(',', '.')
    #         return int(float(val))
    #
    #     invoices = []
    #     if not taxes_iva:
    #         cls.raise_user_error('missing_taxes')
    #         return 0
    #     iva_id = taxes_iva[0].id
    #     ret2_id = taxes_ret2[0].id
    #     ret4_id = taxes_ret4[0].id
    #     ica_id = taxes_ica[0].id
    #     last_number = None
    #     invoice = None
    #     for row in data[5:]:
    #         product_taxes = []
    #         number = row[1]
    #         _type = row[11]
    #
    #         if _type not in  ('Secuencia', 'Impuesto Total'):
    #             continue
    #
    #         lines_to_create = []
    #         if number != last_number:
    #             print(number)
    #             records = Invoice.search([
    #                 ('number', '=', number)
    #             ])
    #             if records:
    #                 continue
    #
    #             count += 1
    #             party_id_number = row[2]
    #             name_party = row[3]
    #             invoice_date = datetime.strptime(row[8], "%d/%m/%Y").date()
    #             email = row[10]
    #             street = ' '
    #             email_to_create = None
    #             if email:
    #                 email_to_create = [('create', [{
    #                     'type': 'email',
    #                     'value': email,
    #                 }])]
    #             country_iso, = Country.search([
    #                 ('code', '=', 'CO')
    #             ])
    #             countries = CountryCode.search([
    #                 ('name', '=', country_iso.name.upper())
    #             ])
    #             country = countries[0]
    #             if not party_id_number:
    #                 cls.raise_user_error('missing_id_party_import', name_party)
    #                 return
    #             if party_id_number[:2] in ('90', '80', '89'):
    #                 type_document = 31
    #             else:
    #                 type_document = 13
    #
    #             if party_id_number not in parties_dict.keys():
    #                 party = Party.search([
    #                     ('id_number', '=', party_id_number)
    #                 ])
    #                 if party:
    #                     party = party[0]
    #                 else:
    #                     party, = Party.create([{
    #                         'name': name_party,
    #                         'id_number': party_id_number,
    #                         'type_document': type_document,
    #                         'regime_tax': '',
    #                         'addresses': [('create', [{
    #                             'name': ' ',
    #                             'country_code': country.id,
    #                             'street': street,
    #                         }])],
    #                         'contact_mechanisms': email_to_create,
    #                     }])
    #                 parties_dict[party_id_number] = party
    #
    #             if number not in invoices_dict.keys():
    #                 count_records.append(1)
    #                 if row[5] == 'BARRANQUILLA':
    #                     shop = 1
    #                 else:
    #                     shop = 2
    #
    #                 invoice_to_create = {
    #                     'number': number,
    #                     'invoice_date': invoice_date,
    #                     'party': parties_dict[party_id_number].id,
    #                     'invoice_address': parties_dict[party_id_number].addresses[0].id,
    #                     'company': 1,
    #                     'payment_term': payment_term.id,
    #                     'state': 'draft',
    #                     'journal': journal_id,
    #                     'type': 'out',
    #                     'account': account_id,
    #                     'lines': [],
    #                     'shop': shop
    #                 }
    #
    #                 invoice, = Invoice.create([invoice_to_create])
    #                 invoices_dict[number] = invoice
    #                 invoice.save()
    #                 invoices.append(invoice)
    #
    #         last_number = number
    #         if _type == 'Impuesto Total' and invoice and row[12] == 'ICA 10 X MIL':
    #             itax = InvoiceTax()
    #             itax.tax = ica_id
    #             itax.invoice = invoice.id
    #             itax.base = text2numeric(row[28])
    #             itax.amount = -1 * text2numeric(row[31])
    #             itax.on_change_tax()
    #             itax.save()
    #             continue
    #
    #         if not row[18]:
    #             continue
    #         qty = int(row[18])
    #         item_product_description = row[13]
    #         item_product_unit_price = row[19]
    #         item_product_unit_price = item_product_unit_price.replace('.', '')
    #         item_product_unit_price = item_product_unit_price.replace(',', '.')
    #
    #         if row[22] == 'IVA 19%':
    #             product_taxes.append(iva_id)
    #
    #         if row[26] == 'RETEFUENTE 4%':
    #             product_taxes.append(ret4_id)
    #
    #         if row[26] == 'RETEFUENTE 2.5%':
    #             product_taxes.append(ret2_id)
    #
    #         all_taxes = [('add', product_taxes)]
    #
    #         if item_product_description:
    #             lines_to_create.append({
    #                 'type': 'line',
    #                 'invoice': invoice.id,
    #                 'description': item_product_description,
    #                 'product': item_product.id,
    #                 'quantity': qty,
    #                 'unit':  item_product.default_uom.id,
    #                 'unit_price': Decimal(item_product_unit_price),
    #                 'taxes': all_taxes,
    #                 'account': item_product.account_revenue_used.id,
    #             })
    #             InvoiceLine.create(lines_to_create)
    #
    #     if invoices:
    #         Invoice.post(invoices)
    #     return count
